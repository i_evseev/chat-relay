import api.discord.DiscordAPIImpl
import api.telegram.TelegramAPIImpl
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select
import queue.MessagesQueueImpl

val queue = MessagesQueueImpl()
val messengers = mutableMapOf(
         Pair("discord", DiscordAPIImpl(queue)),
         Pair("telegram", TelegramAPIImpl(queue))
)

fun main() = runBlocking {
    while (true) {
        select<Unit> {
            queue.queue.onReceive {
                println(it.toString())
            }
        }
    }
}
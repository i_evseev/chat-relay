package api.telegram

import api.API
import queue.MessagesQueue
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.request.SendMessage
import message.TelegramMessageImpl

class TelegramAPIImpl(override val queue: MessagesQueue) : API {

    private var bot: TelegramBot = TelegramBot("771106311:AAH956WRNco6wH4WrsNUszWE_8PaufCJy8o")

    init {
        bot.setUpdatesListener { updates ->
            updates?.forEach {
                queue.add(
                        TelegramMessageImpl(
                                it.message().authorSignature() ?: "none",
                                when (it.message().photo()) {
                                    null -> mutableSetOf<String>()
                                    else -> it.message().photo().map { photo -> photo.fileId() }.toMutableSet()
                                },
                                it.message().date().toLong(),
                                it.message().text() ?: "empty_text"
                        )
                )
            }
            UpdatesListener.CONFIRMED_UPDATES_ALL
        }
    }

    override fun sendMessage(chatId: String, message: String) {
        bot.execute(SendMessage(chatId, message))
    }
}



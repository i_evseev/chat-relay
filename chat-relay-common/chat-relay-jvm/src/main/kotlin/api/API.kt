package api

import queue.MessagesQueue

interface API {
    val queue: MessagesQueue
    fun sendMessage(chatId: String, message: String)
}
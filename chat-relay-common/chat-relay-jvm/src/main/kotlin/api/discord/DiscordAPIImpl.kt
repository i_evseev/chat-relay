package api.discord

import api.API
import queue.MessagesQueue
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.EventListener
import message.DiscordMessageImpl

class DiscordAPIImpl(override val queue: MessagesQueue) : API {

    private var bot: JDA

    init {
        bot = JDABuilder()
                .setToken("NTA2MDQ2MTUwMzE5ODAwMzMx.DuGwbQ.MvO-O1GWEt4ucx8cn66-AIEV_T8")
                .addEventListener(
                        EventListener { event ->
                            if (event is MessageReceivedEvent) onMessageReceived(event)
                        }
                )
                .build()
                .awaitReady()
    }

    override fun sendMessage(chatId: String, message: String) {
        bot.guildCache
                ?.filter {
                    it.textChannelCache?.getElementById(chatId) !== null
                }
                ?.map {
                    it.getTextChannelById(chatId).sendMessage(message).complete()
                }
    }

    private fun onMessageReceived(event: MessageReceivedEvent) {
        val message = event.message
        if (message.author.id == bot.selfUser?.id) return // @TODO проверить, приходят ли уведомления о своих сообщениях
        queue.add(DiscordMessageImpl(
                message.author.name,
                message.attachments.map { it.url }.toMutableSet(),
                message.creationTime.toEpochSecond(),
                message.contentRaw
        ))
    }
}


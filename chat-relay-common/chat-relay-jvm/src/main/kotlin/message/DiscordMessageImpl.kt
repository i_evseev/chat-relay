package message

data class DiscordMessageImpl(
        override var author: String,
        override var attachments: MutableSet<String>,
        override var date: Long,
        override var text: String
) : Message {
    override var source = "discord"
}
package message

data class TelegramMessageImpl(
        override var author: String,
        override var attachments: MutableSet<String>,
        override var date: Long,
        override var text: String
) : Message {
    override var source = "telegram"
}
package message

interface Message {
    val author: String
    val attachments: MutableSet<String>
    val date: Long
    val source: String
    val text: String
}
package queue

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import message.Message

class MessagesQueueImpl : MessagesQueue() {

    override val queue = Channel<Message>(Channel.UNLIMITED)

    override fun add(message: Message) {
        GlobalScope.launch {
            queue.send(message)
        }
    }

}
package queue

import kotlinx.coroutines.channels.Channel
import message.Message

abstract class MessagesQueue {
    abstract val queue: Channel<Message>
    abstract fun add(message: Message)
}